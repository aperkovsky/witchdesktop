﻿using Shell.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Shell.Services
{
    /// <summary>
    /// Dummy employee data service class. Provides dummy data for employees and projects.
    /// Replace with your real employee data access or employee data service proxy.
    /// </summary>
    public class EmployeeDataService : IEmployeeDataService
    {
        private Employee employee;

        public void Authorize()
        {
            throw new NotImplementedException();
        }

        public Employee GetEmployee()
        {
            if (this.employee == null)
            {
                // Dummy Data.
                this.employee = new Employee()
                {
                    Id = "1",
                    Name = "John",
                    LastName = "Smith",
                    Phone = "(425) 555 8912",
                    Email = "John.Smith@Contoso.com"

                };

            }
            return this.employee;

        }
        public bool Login()
        {
            //start Watcher
            return true;
        }
    }
}


