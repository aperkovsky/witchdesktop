﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Shell.Services
{
    /// <summary>
    /// Data service interface.
    /// </summary>
    public interface IEmployeeDataService
    {
        bool Login();
    }
}
