﻿using Hardcodet.Wpf.TaskbarNotification;
using Shell.Services;
using Shell.ViewModels;
using Shell.Views;
using System.Windows;
using System.Windows.Controls.Primitives;
using System.Windows.Input;

namespace Shell.Commands
{
    public class ShowCurrentStatusWindowCommand: CommandBase<ShowCurrentStatusWindowCommand>
    {
        public override void Execute(object parameter)
        {
            //GetTaskbarWindow(parameter).Show();
            var sss = new EmployeeDataService();
            var ss = new StatusViewModel(sss);

            var dd = parameter as TaskbarIcon;
            var sampleWindow = new StatusView(ss);
            dd.ShowCustomBalloon(sampleWindow, PopupAnimation.Slide, 4000);
            //var sampleWindow = new StatusView(ss);

            //sampleWindow.Owner = GetTaskbarWindow(parameter);
            //sampleWindow.WindowStartupLocation = WindowStartupLocation.CenterScreen;
            //sampleWindow.Show();

            CommandManager.InvalidateRequerySuggested();
        }


        public override bool CanExecute(object parameter)
        {
            return true;
            //Window win = GetTaskbarWindow(parameter);
            //return win != null && win.IsVisible;
        }
    }
}
