﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Input;
using Shell.Services;
using Shell.ViewModels;

namespace Shell.Infrastructure.Login.Commands
{
    public class LoginCommand : ICommand
    {
        private LoginViewModel loginViewModel;
        private IEmployeeDataService _employeeDataService;

        public LoginCommand(LoginViewModel loginViewModel, IEmployeeDataService employeeDataService)
        {
            this.loginViewModel = loginViewModel;
            this._employeeDataService = employeeDataService;
        }

        public event EventHandler CanExecuteChanged;

        public bool CanExecute(object parameter)
        {
            return true;
            //throw new NotImplementedException();
        }

        public void Execute(object parameter)
        {
            //should return bool, autorized or not
            _employeeDataService.Login();
           // throw new NotImplementedException();
        }
    }
}
