﻿using Shell.Base;
using Shell.Commands;
using Shell.Infrastructure.Login.Commands;
using Shell.Services;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Input;

namespace Shell.Infrastructure.Login
{
    public class LoginViewModel : ViewModelBase
    {
        private LoginCommand plusCommand;

        public LoginViewModel(IEmployeeDataService dataService)
        {
            plusCommand = new LoginCommand(this, dataService);
        }
        internal void Add()
        {
            throw new NotImplementedException();
        }

        public ICommand AddCommand
        {
            get
            {
                return plusCommand;
                //return new RelayCommand(Add);
            }
        }
    }
}
