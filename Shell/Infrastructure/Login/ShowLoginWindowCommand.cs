﻿using Shell.Commands;
using Shell.Services;
using Shell.ViewModels;
using Shell.Views;
using System.Windows;
using System.Windows.Input;

namespace Shell.Infrastructure.Login
{
    /// <summary>
    /// Shows the main window.
    /// </summary>
    public class ShowLoginWindowCommand : CommandBase<ShowLoginWindowCommand>
    {
        public override void Execute(object parameter)
        {
            //GetTaskbarWindow(parameter).Show();
            var sss = new EmployeeDataService();
            var ss = new LoginViewModel(sss);

            var sampleWindow = new LoginView(ss);

            sampleWindow.Owner = GetTaskbarWindow(parameter);
            sampleWindow.WindowStartupLocation = WindowStartupLocation.CenterScreen;
            sampleWindow.Show();

            CommandManager.InvalidateRequerySuggested();
        }


        public override bool CanExecute(object parameter)
        {
            return true;
            //Window win = GetTaskbarWindow(parameter);
            //return win != null && !win.IsVisible;
        }
    }
}
